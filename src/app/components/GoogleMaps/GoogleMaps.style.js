import styled from 'styled-components';
import { Icon } from 'Elements';

export const Wrapper = styled.div`
	position: relative;
	height: 40rem;
	width: 100%;
`;
export const Marker = styled(Icon)`
	font-size: 4rem;
	position: relative;
	bottom: 4rem;
	right: 1.5rem;
`;

