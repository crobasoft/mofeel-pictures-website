import styled, { css } from 'styled-components';
import { Typography, Colors } from 'Styles';

export default styled.a`
	${props => css`
		font-family: ${Typography.fonts.base};
		color: ${Colors.text.light};
		text-decoration: none;
		transition: 0.2s ease-out;
		padding-bottom: 0;
		text-transform: uppercase;
		font-weight: ${Typography.weights.light};

		&:hover {
			color: ${Colors.primary.base};
			border-color: ${Colors.primary.base};
			cursor: pointer;
		}
		${!props.noBorder && `border-bottom: 1px solid ${Colors.text.base};`}
		${props.isActive && `font-weight: ${Typography.weights.bold};`}
	`};
`;
