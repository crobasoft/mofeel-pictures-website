import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper, WrapperLink } from './Icon.style';

const Icon = ({
	icon,
	link,
	...props
}) => (
	!link ? (<Wrapper {...props}><i className={icon} /></Wrapper>)
		: (<WrapperLink
			href={link}
			{...props}
		>
			<i className={icon} />
		</WrapperLink>)
);

Icon.propTypes = {
	icon: PropTypes.string,
	link: PropTypes.string,
};

export default Icon;
