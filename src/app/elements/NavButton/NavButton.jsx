import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import { NavButtonStyle } from './NavButton.style';

const NavButton = ({
	children,
	to,
	exact,
	...props
}) => (
	<NavButtonStyle {...props}>
		<NavLink
			to={to}
			exact={exact}
		>
			{children}
		</NavLink>
	</NavButtonStyle>
);

NavButton.propTypes = {
	children: PropTypes.node,
	id: PropTypes.string,
	name: PropTypes.string,
	type: PropTypes.string,
	disabled: PropTypes.bool,
	to: PropTypes.string,
	block: PropTypes.bool,
	inverted: PropTypes.bool,
	activeClassName: PropTypes.string,
	exact: PropTypes.bool,
};

export default NavButton;
