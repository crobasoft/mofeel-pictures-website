import styled from 'styled-components';
import { Typography } from 'Styles';
import { TextLink } from 'Elements';

export const NavContainer = styled.div`
	display: flex;
	max-width: 100rem;
	margin: 0 auto;
	justify-content: center;
	padding-bottom: 4rem;
`;

export const NavLink = styled(TextLink)`
	margin: 0 2rem;
	font-size: 1.4rem;
`;
