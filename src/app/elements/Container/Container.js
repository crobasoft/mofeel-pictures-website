import styled from 'styled-components';

export default styled.div`
	margin: 0 auto;
	max-width: ${props => (!props.half ? '150rem' : '75rem')};
	${props => props.centered && 'text-align: center;'}
`;
