import React, { Component } from 'react';
import ModalVideo from 'react-modal-video';
import ScrollAnimation from 'react-animate-on-scroll';

import Header from 'Components/Header';
import Footer from 'Components/Footer';
import VimeoAlbum from 'Components/VimeoAlbum';
import { View, Button, Container, Heading } from 'Elements';
import partnersImgSrc from 'Images/partners.png';

import {
	Banner,
	BottomLinks,
	Section,
	PartnersImg,
} from './Home.style';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modalIsOpen: false,
		};
	}

	openModal = () => {
		this.setState({ modalIsOpen: true });
	}

	closeModal = () => {
		this.setState({ modalIsOpen: false });
	}

	render() {
		return (
			<View>
				<ModalVideo
					channel="vimeo"
					isOpen={this.state.modalIsOpen}
					videoId="241619459"
					onClose={this.closeModal}
				/>
				<Header videoId="260550147">
					<Banner>
						<Banner.Text className="animated fadeInLeft">
							<div>
								<Heading.H3>
									PRODUKTIONSBOLAG
								</Heading.H3>
								<Heading.H6 thin>
									REKLAM &middot; FILM &middot; ANIMATION &middot; DESIGN
								</Heading.H6>
							</div>
						</Banner.Text>
						<Banner.Links className="animated fadeInRight">
							<Banner.Links.Item
								icon="fab fa-facebook-f"
								link="http://facebook.com/mofeelpictures"
							/>
							<Banner.Links.Item
								icon="fab fa-twitter"
								link="https://twitter.com/mofeelpictures"
							/>
							<Banner.Links.Item
								icon="fab fa-instagram"
								link="https://www.instagram.com/mofeelpictures/"
							/>
							<Banner.Links.Item
								icon="fab fa-vimeo-v"
								link="https://vimeo.com/mofeelpictures/"
							/>
						</Banner.Links>
					</Banner>
					<BottomLinks className="animated fadeInUp">
						<BottomLinks.Item
							filled
							inverted
							onClick={this.openModal}
						>
							Se vår film
						</BottomLinks.Item>
						<BottomLinks.Item
							inverted
							to="/contact"
						>
							Kontakt
						</BottomLinks.Item>
					</BottomLinks>
				</Header>

				<Section.WeAre centered>
					<Container>
						<Heading.H3>VI ÄR</Heading.H3>
						<ScrollAnimation animateIn="flipInX">
							<Heading.H1 marginBottom>MOFEEL PICTURES</Heading.H1>
						</ScrollAnimation>
						<p>
							Mofeel Pictures AB är ett produktionsbolag verksamt inom reklam, film, animation och design.
							<br />
							Vi skapar rörligt innehåll till företag, organisationer, föreningar,
							TV-bolag, produktionsbolag, design- och reklambyråer.
						</p>
					</Container>
				</Section.WeAre>

				<Section.WhoAreYou
					centered
					inverted
				>
					<Container>
						<Heading.H3 marginBottom>VILKA ÄR NI?</Heading.H3>
						<div className="row">
							<div className="four columns">
								<ScrollAnimation animateIn="fadeInLeft">
									<Section.Icon icon="fas fa-home" />
									<Heading.H6>FÖRETAG</Heading.H6>
									<p>
										Utifrån ert företags behov tar vi fram rörligt
										<br />
										innehåll och genomför hela projektet
										<br />
										- från idé till slutleverans.
									</p>
								</ScrollAnimation>
							</div>
							<div className="four columns">
								<ScrollAnimation animateIn="fadeInUp">
									<Section.Icon icon="fas fa-bullhorn" />
									<Heading.H6>PRODUKTIONSBOLAG</Heading.H6>
									<p>
										Om ni som produktionsbolag behöver hjälp med postproduktion
										kan vi tillföra de kunskaper och resurser som krävs för att uppnå det förväntade resultatet.
									</p>
								</ScrollAnimation>
							</div>
							<div className="four columns">
								<ScrollAnimation animateIn="fadeInRight">
									<Section.Icon icon="fas fa-comments" />
									<Heading.H6>BYRÅ</Heading.H6>
									<p>
										Har ni en kund som efterfrågar rörligt innehåll?
										<br />
										Vi hjälper er att ta fram det er kund efterfrågar
										<br />
										- från koncept till slutleverans.
									</p>
								</ScrollAnimation>
							</div>
						</div>
						<Button to="/about">
							Vi berättar mer
						</Button>
					</Container>
				</Section.WhoAreYou>

				<Section.CheckOut centered>
					<Container>
						<Heading.H3 marginBottom>KOLLA HÄR!</Heading.H3>
						<VimeoAlbum
							albumId="5054060"
							maxVideos={12}
							hideTitles
						/>
						<Button to="/portfolio">
							Se fler filmer
						</Button>
					</Container>
				</Section.CheckOut>

				<Section.WeHelpWith
					centered
					inverted
				>
					<Container>
						<Heading.H5 marginBottom>VI HJÄLPER ER MED</Heading.H5>
						<ScrollAnimation animateIn="fadeInUp">
							<div className="row">
								<div className="three columns">
									<Section.Icon icon="fas fa-search" />
									<Heading.H6>FÖRPRODUKTION</Heading.H6>
									<p>
										PROJEKTERING • PLANERING • ANALYS
										<br />
										MANUS • STORYBOARD
									</p>
								</div>
								<div className="three columns">
									<Section.Icon icon="fas fa-video" />
									<Heading.H6>PRODUKTION</Heading.H6>
									<p>
										FILMINSPELNING • FOTO • TIMELAPSE • SFX
										<br />
										STOPMOTION • STUDIO • LJUDUPPTAGNING
									</p>
								</div>
								<div className="three columns">
									<Section.Icon icon="fas fa-cut" />
									<Heading.H6>POSTPRODUKTION</Heading.H6>
									<p>
										KLIPPNING • IHOPSÄTTNING • ANIMATION
										<br />
										COLOR GRADING • LJUDMIX • LJUDLÄGGNING
									</p>
								</div>
								<div className="three columns">
									<Section.Icon icon="fas fa-fire" />
									<Heading.H6>AVANCERAD POSTPRODUKTION</Heading.H6>
									<p>
										VFX • MOTION DESIGN • 3D
										<br />
										VR • COMP
									</p>
								</div>
							</div>
						</ScrollAnimation>
						<Button to="/about">
							Vi berättar mer
						</Button>
					</Container>
				</Section.WeHelpWith>

				<Section.WeCreateFor centered>
					<Container>
						<Heading.H5 marginBottom>VI SKAPAR INNEHÅLL TILL</Heading.H5>
						<ScrollAnimation animateIn="fadeIn">
							<PartnersImg
								alt=""
								src={partnersImgSrc}
							/>
						</ScrollAnimation>
					</Container>
				</Section.WeCreateFor>

				<Footer />
			</View>
		);
	}
}
export default Home;
