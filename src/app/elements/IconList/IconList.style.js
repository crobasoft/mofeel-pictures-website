import styled from 'styled-components';
import { Colors, Typography } from 'Styles';

const List = styled.ul`
	list-style: none;
	display: inline-block;
`;

const ListItem = styled.li`
	text-align: left;
	display: flex;
	align-items: center;
	margin-bottom: 0;
`;
ListItem.Text = styled.span`
	border-left: 1px solid ${Colors.text.base};
	padding: 1rem 1.5rem;
	font-weight: ${Typography.weights.light};
	display: block;

	@media (min-width: 700px) {
		padding: 1rem 4rem;
	}
`;
ListItem.Icon = styled.i`
	margin-right: 1.5rem;
	width: 2rem !important;

	@media (min-width: 700px) {
		margin-right: 4rem;
	}
`;

export {
	List,
	ListItem,
};
