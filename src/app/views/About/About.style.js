import styled from 'styled-components';
import { Typography, Colors } from 'Styles';
import { Icon } from 'Elements';

export const SectionIcon = styled(Icon)`
	color: ${Colors.primary.base};
	font-size: 4rem;
	margin-bottom: 1.5rem;
`;
export const ComputerImg = styled.img`
	max-width: 100rem;
	width: 100%;
	margin-top: 6rem;
`;
export const IconList = styled.div`
	text-align: center;
	${props => props.marginTop && 'margin-top: 4rem;'}
	${props => props.smallText && 'font-size: 1.5rem;'}
	p {
		font-weight: ${Typography.weights.light};
		margin-top: 2rem;
		font-size: 1.5rem;
	}
`;
export const SectionRow = styled.div`
	button {
		margin-top: 4rem;
	}
`;

export const AboutVideo = styled.iframe`
	width: 100%;
	@media (max-width: 700px) {
		height: 20rem;
		margin-top: 6rem;
	}
	@media (min-width: 701px) and (max-width: 1000px) {
		height: 30rem;
		margin-top: 6rem;
	}
`;
