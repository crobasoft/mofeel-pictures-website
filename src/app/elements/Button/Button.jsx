import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { ButtonStyle, ButtonText } from './Button.style';

const Button = ({
	children,
	to,
	href,
	inverted,
	...props
}) => {
	if (to || href) {
		return (
			<Link
				to={to}
				href={href}
			>
				<ButtonStyle
					inverted={inverted}
					{...props}
				>
					<ButtonText>
						{children}
					</ButtonText>
				</ButtonStyle>
			</Link>
		);
	}
	return (
		<ButtonStyle
			inverted={inverted}
			{...props}
		>
			<ButtonText>
				{children}
			</ButtonText>
		</ButtonStyle>
	);
};

Button.propTypes = {
	children: PropTypes.node,
	id: PropTypes.string,
	name: PropTypes.string,
	type: PropTypes.string,
	disabled: PropTypes.bool,
	onClick: PropTypes.func,
	to: PropTypes.string,
	href: PropTypes.string,
	block: PropTypes.bool,
	inverted: PropTypes.bool,
	filled: PropTypes.bool,
};

export default Button;
