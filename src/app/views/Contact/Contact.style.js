import styled from 'styled-components';
import { Colors } from 'Styles';
import { Icon } from 'Elements';

const BigIcon = styled(Icon)`
	color: ${Colors.primary.base};
	font-size: 5rem;
`;

const IconGrid = styled.div`
	text-align: center;
	${props => props.marginTop && 'margin-top: 4rem;'}
`;
IconGrid.Icon = styled(Icon)`
	color: ${Colors.primary.base};
	font-size: 3rem;
`;

const NameAndJob = styled.div`
	margin-left: 6rem;
`;

export {
	BigIcon,
	IconGrid,
	NameAndJob,
};
