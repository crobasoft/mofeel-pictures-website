import React from 'react';
import PropTypes from 'prop-types';

import IconListItem from './IconListItem';
import { List } from './IconList.style';

const IconList = ({ children, ...props }) => <List {...props}>{children}</List>;

IconList.Item = IconListItem;

IconList.propTypes = {
	children: PropTypes.node,
};
export default IconList;
