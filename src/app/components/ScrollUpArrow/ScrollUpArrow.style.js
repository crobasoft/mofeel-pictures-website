import styled from 'styled-components';
import { Icon } from 'Elements';
import { Colors, Typography } from 'Styles';

export const Wrapper = styled.div`
	position: fixed;
	bottom: 0;
	left: 0;
	width: 100%;
	display: flex;
	justify-content: flex-end;
	z-index: 50;
`;
export const ArrowContainer = styled.div`
	padding: 1rem;
	display: flex;
	align-items: center;

	&:hover {
		cursor: pointer;
		span {
			opacity: 1;
		}
	}
`;
export const ArrowText = styled.span`
	opacity: 0;
	font-size: 1.5rem;
	font-weight: ${Typography.weights.light};
	margin-right: 1rem;
	background-color: ${Colors.primary.base};
	color: #fff;
	padding: 0.5rem;
	pointer-events: none;
	transition: 0.2s ease;
`;
export const Arrow = styled(Icon)`
	font-size: 4rem;
	color: ${Colors.primary.base};
`;
