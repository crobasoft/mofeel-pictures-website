import React from 'react';
import ScrollAnimation from 'react-animate-on-scroll';

import Header from 'Components/Header';
import Footer from 'Components/Footer';
import GoogleMaps from 'Components/GoogleMaps';
import {
	View,
	Container,
	PromoSection,
	IconList,
	Heading,
	Paragraph,
	Section,
	Flex,
} from 'Elements';

import {
	BigIcon,
	IconGrid,
	NameAndJob,
} from './Contact.style';

const Contact = () => (
	<View>
		<Header />

		<PromoSection>
			<Container centered>
				<BigIcon
					icon="fas fa-map-marker-alt"
					className="animated bounceInDown"
				/>
				<Heading.H5 marginBottom>KONTAKTA OSS</Heading.H5>
				<IconList>
					<IconList.Item icon="fas fa-phone">
						+46 (0)76 708 28 46
					</IconList.Item>
					<IconList.Item icon="far fa-envelope">
						hej@mofeelpictures.se
					</IconList.Item>
					<IconList.Item icon="fas fa-globe">
						www.mofeelpictures.se
					</IconList.Item>
					<IconList.Item icon="fas fa-map-marker-alt">
						Turingevägen 41b, 122 43 Älvsjö
					</IconList.Item>
				</IconList>
			</Container>
		</PromoSection>

		<Container>
			<Flex justifyContent="center">
				<Flex.Box
					className="animated fadeInLeft"
					padded
				>
					<NameAndJob>
						<Heading.H6>Felix Ahlberg</Heading.H6>
						<Paragraph
							small
							light
							scheme="primary"
						>
							VD & producent
						</Paragraph>
					</NameAndJob>
					<IconList>
						<IconList.Item icon="fas fa-phone">
							+46 (0)76 708 28 46
						</IconList.Item>
						<IconList.Item icon="far fa-envelope">
							felix@mofeelpictures.se
						</IconList.Item>
					</IconList>
				</Flex.Box>
				<Flex.Box
					className="animated fadeInRight"
					padded
				>
					<NameAndJob>
						<Heading.H6>Emelie Blomqvist</Heading.H6>
						<Paragraph
							small
							light
							scheme="primary"
						>
							Projektledare
						</Paragraph>
					</NameAndJob>
					<IconList>
						<IconList.Item icon="fas fa-phone">
							+46 (0)72 264 03 05
						</IconList.Item>
						<IconList.Item icon="far fa-envelope">
							emelie@mofeelpictures.se
						</IconList.Item>
					</IconList>
				</Flex.Box>
			</Flex>
		</Container>

		<GoogleMaps
			latitude={59.3293235}
			longitude={18.0685808}
			markerCoordinates={{
				latitude: 59.273544,
				longitude: 18.019631,
			}}
			zoom={11}
		/>

		<PromoSection>
			<Container
				half
				centered
			>
				<ScrollAnimation animateIn="bounceInUp">
					<IconGrid className="row">
						<div className="three columns">
							<IconGrid.Icon
								icon="fab fa-facebook-f"
								link="http://facebook.com/mofeelpictures"
							/>
						</div>
						<div className="three columns">
							<IconGrid.Icon
								icon="fab fa-instagram"
								link="https://www.instagram.com/mofeelpictures/"
							/>
						</div>
						<div className="three columns">
							<IconGrid.Icon
								icon="fab fa-twitter"
								link="https://twitter.com/mofeelpictures"
							/>
						</div>
						<div className="three columns">
							<IconGrid.Icon
								icon="fab fa-vimeo-v"
								link="https://vimeo.com/mofeelpictures/"
							/>
						</div>
					</IconGrid>
				</ScrollAnimation>
				<Flex
					justifyContent="center"
					alignItems="center"
				>
					<Flex.Box paddedTop>
						<Section>
							<Heading.H6 marginBottom>Söker du jobb eller praktik?</Heading.H6>
							<Paragraph noMargin>Kontakta oss på:</Paragraph>
							<Paragraph light>jobb@mofeelpictures.se</Paragraph>
						</Section>
					</Flex.Box>
				</Flex>
			</Container>
		</PromoSection>

		<Footer />
	</View>
);
export default Contact;
