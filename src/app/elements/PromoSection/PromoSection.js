import styled, { css } from 'styled-components';
import { Colors } from 'Styles';

export default styled.div`
	${props => css`
		padding: 5rem;
		${props.centered && 'text-align: center;'}
		${props.inverted && `background-color: ${Colors.default.base}`}
	`}
`;
