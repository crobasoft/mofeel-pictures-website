import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from 'Views/Home';
import Portfolio from 'Views/Portfolio';
import About from 'Views/About';
import Contact from 'Views/Contact';
import NotFound from 'Views/NotFound';
import StartAnimation from 'Components/StartAnimation';

const App = () => (
	<div>
		<StartAnimation />
		<Router>
			<Switch>
				<Route
					exact
					path="/"
					component={Home}
				/>
				<Route
					path="/portfolio"
					component={Portfolio}
				/>
				<Route
					path="/about"
					component={About}
				/>
				<Route
					path="/contact"
					component={Contact}
				/>
				<Route
					component={NotFound}
				/>
			</Switch>
		</Router>
	</div>
);
export default App;
