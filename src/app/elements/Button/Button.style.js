import styled, { css } from 'styled-components';

import { Typography, Colors } from 'Styles';

const getBgColor = inverted => {
	if (inverted) {
		return Colors.primary.base;
	}
	return Colors.text.base;
};

const getStyle = props => css`
	position: relative;
	font-family: ${Typography.fonts.base};
	font-size: 1.6rem;
	font-weight: ${Typography.weights.light};
	padding: 0.75rem 2rem;
	border: 0;
	background: ${props.filled ? getBgColor(props.inverted) : 'transparent'};
	cursor: pointer;
	color: ${props.inverted ? '#fff' : Colors.text.base};
	margin: 0;
	white-space: normal;
	word-wrap: break-word;
	text-transform: uppercase;
	text-decoration: none;
	${props.block && 'width: 100%;'}

	&:before {
		content: '';
		position: absolute;
		top: -0.2rem;
		right: -0.2rem;
		bottom: -0.2rem;
		left: -0.2rem;
		transition: 0.2s ease;
		border: 0.2rem solid ${getBgColor(props.inverted)};
	}

	&:hover {
		background-color: ${getBgColor(props.inverted)};
		color: #fff;
		&:before {
			top: -0.7rem;
			right: -0.7rem;
			bottom: -0.7rem;
			left: -0.7rem;
		}
		> span {
			transform: scale(1.03);
		}
	}
	&:focus {
		outline: 0;
	}
`;

export const ButtonStyle = styled.button`
	${props => getStyle(props)}
`;

export const ButtonText = styled.span`
	display: block;
	transition: 0.2s ease;
`;
