import styled from 'styled-components';
import { Typography, Colors } from 'Styles';
import { Icon, Button, PromoSection } from 'Elements';

const Banner = styled.div`
	flex-grow: 1;
	display: flex;
	padding: 0 5rem;
	width: 100%;
	color: #fff;
	transition: 0.2s ease-out;
`;
Banner.Text = styled.div`
	flex-grow: 1;
	display: flex;
	align-items: center;
	@media (max-width: 1000px) {
		display: none;
	}
`;
Banner.Links = styled.div`
	display: flex;
	flex-direction: column;
	@media (min-width: 501px) and (max-width: 1000px) {
		margin-bottom: 5rem;
	}
	@media (max-width: 500px) {
		margin-bottom: 3rem;
	}
	@media (max-width: 1000px) {
		flex-direction: row;
		justify-content: center;
		width: 100%;
	}
`;
Banner.Links.Item = styled(Icon)`
	padding: 1rem 0;
	@media (max-width: 1000px) {
		padding: 0 2rem;
	}
`;

const BottomLinks = styled.div`
	display: flex;
	width: 100%;
	margin-bottom: 5rem;
	justify-content: center;
	position: absolute;
	bottom: 0;
	left: 0;
	@media (max-width: 500px) {
		margin-bottom: 2rem;
	}
`;
BottomLinks.Item = styled(Button)`
	margin: 0 2rem;
`;

const Section = () => {};

Section.Icon = styled(Icon)`
	color: ${Colors.primary.base};
	font-size: 4rem;
	margin-bottom: 1.5rem;
`;
Section.WeAre = styled(PromoSection)`
	h1 {
		line-height: 0.8;
	}
	h3 {
		color: ${Colors.primary.base};
	}
`;
Section.WhoAreYou = styled(PromoSection)`
	p {
		margin-top: 2rem;
		font-weight: ${Typography.weights.light};
	}
	button {
		margin-top: 2rem;
	}
`;
Section.CheckOut = styled(PromoSection)`
	button {
		margin-top: 2rem;
	}
`;
Section.WeHelpWith = styled(PromoSection)`
	p {
		font-weight: ${Typography.weights.light};
		margin-top: 2rem;
		font-size: 1.5rem;
	}
	button {
		margin-top: 2rem;
	}
`;
Section.WeCreateFor = styled(PromoSection)`
`;

const PartnersImg = styled.img`
	max-width: 100rem;
	width: 100%;
	margin-top: 5rem;
`;

export {
	Banner,
	BottomLinks,
	Section,
	PartnersImg,
};
