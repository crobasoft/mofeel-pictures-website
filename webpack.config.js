const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const env = process.env.NODE_ENV;
const BUILD_DIR = path.resolve(__dirname, './dist');
const SRC_DIR = path.resolve(__dirname, './src');

const config = {
	mode: env || 'development',
	entry: {
		'bundle.min': `${SRC_DIR}/index.jsx`,
	},
	devtool: 'source-map',
	output: {
		path: BUILD_DIR,
		filename: '[name].js',
	},
	performance: {
		hints: false,
	},
	module: {
		rules: [
			{
				test: /\.jsx$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.(png|jp(e*)g|svg)$/,
				use: [{
					loader: 'url-loader',
					options: {
						limit: 8000, // Convert images < 8kb to base64 strings
						name: 'img/[hash]-[name].[ext]',
					},
				}],
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
		],
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			Views: `${SRC_DIR}/app/views`,
			Components: `${SRC_DIR}/app/components`,
			Elements: `${SRC_DIR}/app/elements`,
			Styles: `${SRC_DIR}/app/styles`,
			Images: `${SRC_DIR}/app/img`,
		},
	},
	plugins: [
		new HTMLWebpackPlugin({
			template: `${SRC_DIR}/index.html`,
			filename: 'index.html',
			inject: 'body',
		}),
		new UglifyJsPlugin({
			include: /\.min\.js$/,
		}),
	],
};

module.exports = config;
