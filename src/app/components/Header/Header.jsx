import React from 'react';
import PropTypes from 'prop-types';
import { NavButton, Logo, VideoBackground } from 'Elements';

import {
	Wrapper,
	Content,
	NestedContent,
	Navigation,
	NavigationLinks,
	LinkItem,
	NavigationExtraContainer,
	NavigationLogoContainer,
	VideoWrapper,
} from './Header.style';

const Header = ({ children, videoId }) => (
	<Wrapper hasVideo={!!videoId}>
		<Content hasVideo={!!videoId}>
			<Navigation className="row">
				<NavigationExtraContainer className="five columns" />
				<NavigationLogoContainer className="two columns">
					<Logo white={!!videoId} />
				</NavigationLogoContainer>
				<NavigationLinks className="five columns">
					<LinkItem>
						<NavButton
							to="/"
							exact
							inverted={videoId && true}
							activeClassName="active"
						>
							Hem
						</NavButton>
					</LinkItem>
					<LinkItem>
						<NavButton
							inverted={videoId && true}
							activeClassName="active"
							to="/portfolio"
						>
							Portfolio
						</NavButton>
					</LinkItem>
					<LinkItem>
						<NavButton
							inverted={videoId && true}
							activeClassName="active"
							to="/about"
						>
							Om oss
						</NavButton>
					</LinkItem>
					<LinkItem>
						<NavButton
							inverted={videoId && true}
							activeClassName="active"
							to="/contact"
						>
							Kontakt
						</NavButton>
					</LinkItem>
				</NavigationLinks>
			</Navigation>
			<NestedContent>
				{children}
			</NestedContent>
		</Content>
		{videoId &&
			<VideoWrapper>
				<VideoBackground videoId={videoId} />
			</VideoWrapper>
		}
	</Wrapper>
);
Header.propTypes = {
	children: PropTypes.node,
	videoId: PropTypes.string,
};
export default Header;
