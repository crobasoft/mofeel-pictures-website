export default {
	fonts: {
		base: '"Roboto", sans-serif',
	},
	sizes: {
		text: {
			base: '18px',
			lg: '20px',
			md: '16px',
			sm: '14px',
			xs: '12px',
		},
		heading: {
			h1: '36px',
			h2: '27px',
			h3: '21px',
			h4: '20px',
			h5: '18px',
			h6: '18px',
		},
	},
	weights: {
		thin: '100',
		light: '300',
		regular: '400',
		bold: '700',
	},
};
