import React from 'react';
import PropTypes from 'prop-types';

import {
	ThumbnailContainer,
	ThumbnailImageContainer,
	ThumbnailImage,
	ThumbnailTitle,
} from './VimeoAlbum.style';

const VimeoThumbnail = ({ title, imgSrc, onSelect }) => (
	<ThumbnailContainer>
		<ThumbnailImageContainer onClick={onSelect}>
			<ThumbnailImage src={imgSrc} />
		</ThumbnailImageContainer>
		{title &&
			<ThumbnailTitle>{title}</ThumbnailTitle>
		}
	</ThumbnailContainer>
);
VimeoThumbnail.propTypes = {
	title: PropTypes.string,
	imgSrc: PropTypes.string,
	onSelect: PropTypes.func,
};
export default VimeoThumbnail;
