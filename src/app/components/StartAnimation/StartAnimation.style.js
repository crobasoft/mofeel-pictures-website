import styled from 'styled-components';

export const Wrapper = styled.div`
	background-color:#fff;
	width: 100%;
	height: 100%;
	display: block;
	position: fixed;
	top: 0;
	left: 0;
	z-index: 99;
	overflow: hidden;
	transform: translate3d(0,0,0);
	text-align: center;
	opacity: 1;
`;

export const Animation = styled.div`
	opacity: ${props => (props.isReady ? '1' : '0')};
`;
