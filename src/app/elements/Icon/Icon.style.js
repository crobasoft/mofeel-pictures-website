import styled, { css, keyframes } from 'styled-components';
import { Colors } from 'Styles';

const rotate360 = keyframes`
	from { transform: rotate(0deg); }
	to { transform: rotate(360deg); }
`;

const getStyle = props => css`
	font-size: 2rem;
	transition: 0.2s ease-out;
	${props.spin && `animation: ${rotate360} 2s linear infinite;`}
`;

export const Wrapper = styled.div`
	${props => getStyle(props)}
`;

export const WrapperLink = styled.a`
	${props => getStyle(props)}
	color: ${props => (props.scheme ? Colors[props.scheme][props.variant || 'base'] : '#fff')};
	&:hover {
		color: ${Colors.primary.base};
		transform: scale(1.5);
		cursor: pointer;
	}
`;
