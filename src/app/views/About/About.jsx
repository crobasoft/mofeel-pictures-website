import React from 'react';

import Header from 'Components/Header';
import Footer from 'Components/Footer';
import { View, Container, PromoSection, Button, Heading } from 'Elements';
import computerImgSrc from 'Images/computer.jpg';

import {
	IconList,
	SectionIcon,
	ComputerImg,
	SectionRow,
	AboutVideo,
} from './About.style';

const About = () => (
	<View>
		<Header />

		<PromoSection>
			<Container>
				<SectionRow className="row">
					<div className="seven columns">
						<Heading.H5 marginBottom>VI ÄR MOFEEL PICTURES</Heading.H5>
						<p>
							Mofeel Pictures AB är ett produktionsbolag verksamt inom reklam, film, animation och design.
						</p>
						<p>
							Vi skapar rörligt innehåll till företag, organisationer, föreningar, produktionsbolag,
							TV-bolag, design- och reklambyråer.
						</p>
						<p>
							I 10 års tid har vi levererat rörligt innehåll för olika typer av medier och plattformar.
							Vi levererar årligen över 100 produktioner i olika längder och format.
						</p>
						<Button
							to="/contact"
							className="animated flipInX"
						>
							Kontakt
						</Button>
					</div>
					<div className="five columns">
						<AboutVideo
							title="260550147"
							src="https://player.vimeo.com/video/260550147?background=1&loop=1"
							width="960"
							height="540"
							frameBorder="0"
							webkitallowfullscreen
							mozallowfullscreen
							allowFullscreen
						/>
					</div>
				</SectionRow>
			</Container>
		</PromoSection>

		<PromoSection inverted>
			<Container>
				<SectionRow className="row">
					<div className="seven columns">
						<Heading.H5 marginBottom>FÖRETAGSFILM</Heading.H5>
						<p>
							Utifrån ert företags behov tar vi fram rörligt innehåll och genomför hela projektet
							<br />
							- från idé till slutleverans.
						</p>
						<p>
							Vi hjälper våra kunder med instruktions-, utbildnings-, informations- och reklamfilm
							för att kommunicera internt i företaget eller externt till företagets kunder eller partners.
						</p>
						<p>
							För ett lyckat slutresultat är tydlig dialog och kommunikation med er kring planering
							och genomförande av projektet minst lika viktigt som snygga bilder, formspråk, färg och rörelse i slutresultatet.
						</p>
						<p>
							Ni är varmt välkomna att kontakta oss så att vi kan prata mer om era tankar, idéer och önskemål!
						</p>
					</div>
					<div className="five columns">
						<IconList className="row">
							<div className="four columns">
								<SectionIcon icon="fas fa-phone" />
								<b>KONTAKT</b>
								<p>
									Kontakta oss gärna och berätta om era behov.
								</p>
							</div>
							<div className="four columns">
								<SectionIcon icon="fas fa-coffee" />
								<b>MÖTE</b>
								<p>
									Vi ses på ett möte där vi sätter ramarna för projektet.
								</p>
							</div>
							<div className="four columns">
								<SectionIcon icon="fas fa-search" />
								<b>FÖRPRODUKTION</b>
								<p>
									Planerar och skapar bild och ljudmanus.
								</p>
							</div>
						</IconList>
						<IconList className="row">
							<div className="four columns">
								<SectionIcon icon="fas fa-film" />
								<b>VI SKAPAR INNEHÅLLET</b>
								<p>
									Vi skapar och levererar innehållet.
								</p>
							</div>
							<div className="four columns">
								<SectionIcon icon="fas fa-wrench" />
								<b>FEEDBACK</b>
								<p>
									Korrigeringar? Inga problem!
								</p>
							</div>
							<div className="four columns">
								<SectionIcon icon="fas fa-dolly" />
								<b>LEVERANS</b>
								<p>
									Slutleverans när alla är nöjda.
								</p>
							</div>
						</IconList>
					</div>
				</SectionRow>
			</Container>
		</PromoSection>

		<PromoSection>
			<Container>
				<SectionRow className="row">
					<div className="seven columns">
						<Heading.H5 marginBottom>AVANCERAD POSTPRODUKTION</Heading.H5>
						<p>
							Lika ofta som vi hjälper företag att ta fram rörligt innehåll hjälper vi produktionsbolag,
							TV-bolag, design- och reklambyråer med det vi kallar avancerad postproduktion.
						</p>
						<p>
							Vi anlitas ofta som räddande änglar inom områdena VFX, animation, motion design och 3D.
						</p>
						<p>
							Oavsett om ni behöver hjälp med hela produktionen eller bara vissa delar kan vi tillföra
							de kunskaper och resurser som krävs för att uppnå det förväntade resultatet.
						</p>
						<IconList
							marginTop
							className="row"
							smallText
						>
							<div className="three columns">
								<SectionIcon icon="fas fa-search" />
								<b>FÖRPRODUKTION</b>
							</div>
							<div className="three columns">
								<SectionIcon icon="fas fa-video" />
								<b>PRODUKTION</b>
							</div>
							<div className="three columns">
								<SectionIcon icon="fas fa-cut" />
								<b>POSTPRODUKTION</b>
							</div>
							<div className="three columns">
								<SectionIcon icon="fas fa-fire" />
								<b>AVANCERAD POSTPRODUKTION</b>
							</div>
						</IconList>
					</div>
					<div className="five columns">
						<ComputerImg
							src={computerImgSrc}
							alt=""
						/>
					</div>
				</SectionRow>
			</Container>
		</PromoSection>

		<Footer />
	</View>
);
export default About;
