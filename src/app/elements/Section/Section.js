import styled from 'styled-components';

export default styled.section`
	${props => props.centered && 'text-align: center;'}
`;
