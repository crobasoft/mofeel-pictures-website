import React, { Component } from 'react';

import Header from 'Components/Header';
import Footer from 'Components/Footer';
import VimeoAlbum from 'Components/VimeoAlbum';
import { View } from 'Elements';

import { NavContainer, NavLink } from './Portfolio.style';

const albums = [
	{
		title: 'Animation',
		albumId: '5054062',
	},
	{
		title: 'Film',
		albumId: '3719626',
	},
	{
		title: 'Motion Design',
		albumId: '3719631',
	},
	{
		title: 'VFX',
		albumId: '3719630',
	},
	{
		title: 'Så gjorde vi',
		albumId: '5054061',
	},
];

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentAlbum: albums[0],
		};
	}

	selectAlbum = album => this.setState({ currentAlbum: album });

	render() {
		return (
			<View>
				<Header />

				<NavContainer className="animated fadeInUp">
					{albums.map(album => (
						<NavLink
							key={album.albumId}
							noBorder
							isActive={this.state.currentAlbum.albumId === album.albumId}
							onClick={() => this.selectAlbum(album)}
						>
							{album.title}
						</NavLink>
					))}
				</NavContainer>

				<VimeoAlbum albumId={this.state.currentAlbum.albumId} />

				<Footer />
			</View>
		);
	}
}
export default Home;
