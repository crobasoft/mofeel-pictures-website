export default {
	default: {
		base: '#EAEAEA',
		dark: '#C3C3C3',
	},
	text: {
		base: '#393939',
		light: '#636c72',
	},
	primary: {
		base: '#AE4242',
		dark: '#C00D1E',
	},
	secondary: {
		base: '#0074C3',
		dark: '#00498E',
	},
	success: {
		base: '#30BE3C',
		light: '#008C44',
		dark: '#bae1c1',
	},
	danger: {
		base: 'rgb(217, 83, 79)',
		dark: 'darken(rgb(217, 83, 79), 10%)',
	},
	warning: {
		base: '#F9DC06',
		light: '#FBEDB4',
		dark: '#F8C30E',
	},
	info: {
		base: '#0074C3',
		dark: 'darken(#0074C3, 10%)',
	},
	gray: {
		dark: '#ddd',
	},
};
