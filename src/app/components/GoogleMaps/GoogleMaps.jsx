import React from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';

import { Wrapper, Marker } from './GoogleMaps.style';

const setupMapOptions = () => ({
	disableDefaultUI: true,
});

const GoogleMaps = ({
	markerCoordinates,
	latitude,
	longitude,
	zoom,
}) => (
	<Wrapper>
		<GoogleMapReact
			bootstrapURLKeys={{ key: 'AIzaSyCplWjRE6L8Wks_nVkATExBgPJJHOlygFs' }}
			defaultZoom={zoom}
			defaultCenter={{ lat: latitude, lng: longitude }}
			options={setupMapOptions}
		>
			<Marker
				icon="fas fa-map-marker-alt animated bounceInDown"
				lat={markerCoordinates.latitude}
				lng={markerCoordinates.longitude}
			/>
		</GoogleMapReact>
	</Wrapper>
);

GoogleMaps.propTypes = {
	latitude: PropTypes.number,
	longitude: PropTypes.number,
	markerCoordinates: PropTypes.shape({
		latitude: PropTypes.number,
		longitude: PropTypes.number,
	}),
	zoom: PropTypes.number,
};
GoogleMaps.defaultProps = {
	zoom: 8,
};
export default GoogleMaps;
