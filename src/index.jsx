import React from 'react';
import { render } from 'react-dom';
import ModalVideo from '../node_modules/react-modal-video/css/modal-video.min.css'; // eslint-disable-line
import AnimateCSS from './app/css/animate.css'; // eslint-disable-line
import Skeleton from './app/css/skeleton.css'; // eslint-disable-line
import Normalize from './app/css/normalize.css'; // eslint-disable-line
import GlobalStyles from 'Styles/global'; // eslint-disable-line
import App from './app/App';

const appElement = document.getElementById('app');
if (appElement) {
	render(<App/>, appElement);
}
