import styled from 'styled-components';
import { Typography } from 'Styles';

export const VimeoAlbumContainer = styled.div`
	display: flex;
	flex-wrap: wrap;
	margin: 0 auto;
	max-width: 100rem;
	position: relative;
	min-height: 10rem;
`;
export const LoadingContainer = styled.div`
	pointer-events: none;
	position: absolute;
	width: 100%;
	height: 100%;
	background-color: rgba(255,255,255,0.9);
	text-align: center;
	z-index: 2;
	transition: 0.4s ease-out;

	div {
		margin-bottom: 2rem;
	}
`;
export const ErrorContainer = styled.div`
	text-align: center;
`;

export const ThumbnailContainer = styled.div`
	position: relative;
	width: 100%;
	padding: 2rem;
	transition: 0.2s ease;
	opacity: 0.7;
	&:hover {
		opacity: 1;
		cursor: pointer;
		transform: translateY(-1rem);
	}

	@media (min-width: 700px) {
		width: 33%;
	}
`;
export const ThumbnailImageContainer = styled.div`
	position: relative;
	padding-bottom: 56.25%;
	width: 100%;
	height: auto;
	overflow: hidden;
	background-color: #000;
`;
export const ThumbnailImage = styled.img`
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: auto;
	transition: 0.2s ease;
`;
export const ThumbnailTitle = styled.div`
	font-size: 1.4rem;
	font-weight: ${Typography.weights.light};
	transition: 0.2s ease;
	margin-top: 1rem;
`;
