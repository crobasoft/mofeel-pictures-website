export default {
	borderRadius: '5px',
	margin: {
		xs: '1rem',
		sm: '2rem',
		md: '3rem',
		lg: '4rem',
		xlg: '5rem',
	},
	padding: {
		xs: '1rem',
		sm: '2rem',
		md: '3rem',
		lg: '4rem',
		xlg: '5rem',
	},
};
