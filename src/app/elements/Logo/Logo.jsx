import React from 'react';
import PropTypes from 'prop-types';

import logoWhite from 'Images/logoWhite.png';
import logoBlack from 'Images/logoBlack.png';

import { LogoStyle } from './Logo.style';

const Logo = ({ white }) => (
	<LogoStyle
		src={white ? logoWhite : logoBlack}
		alt=""
	/>
);
Logo.propTypes = {
	white: PropTypes.bool,
};
export default Logo;
