import styled from 'styled-components';
import videoOverlay from 'Images/videoOverlay.png';

export const VideoBackgroundStyle = styled.div`
	position: relative;
	padding-bottom: 56.25%;
	max-width: 100%;
	height: auto;
	overflow: hidden;
`;

export const VideoStyle = styled.iframe`
	position: absolute;
	z-index: 0;
	width: 100%;
	height: 100%;
`;

export const VideoBackgroundOverlay = styled.div`
	position: absolute;
	left: 0;
	top: 0;
	z-index: 1;
	pointer-events: none;
	background: url(${videoOverlay});
	width: 100%;
	height: 100%;
`;
