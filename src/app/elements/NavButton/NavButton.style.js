/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';
import { Typography, Colors } from 'Styles';

export const NavButtonStyle = styled.span`
	a {
		position: relative;
		font-family: ${Typography.fonts.base};
		font-size: 1.6rem;
		font-weight: ${Typography.weights.thin};
		padding: 0 0 0.1rem 0;
		border: 0;
		background: transparent;
		cursor: pointer;
		color: ${Colors.default.dark};
		margin: 0;
		white-space: normal;
		word-wrap: break-word;
		text-transform: uppercase;
		${props => props.block && 'width: 100%;'}
		transition: 0.2s ease-out;
		text-decoration: none;
		&.active {
			font-weight: ${Typography.weights.regular};
			color: ${props => (props.inverted ? '#fff' : Colors.text.base)};
			border-bottom: 0.2rem solid ${Colors.primary.base};
		}
		&:hover {
			${props => !props.active && `
				color: ${Colors.primary.base};
				transform: scale(1.05);
			`}
		}
		&:focus {
			outline: 0;
		}
	}
`;
