import styled, { css } from 'styled-components';
import { Colors, Typography } from 'Styles';

const Heading = () => {};

const baseStyle = css`
	${props => css`
		font-family: ${Typography.fonts.base};
		margin: 0;
		${!props.thin && `font-weight: ${Typography.weights.bold}`};
		${props.marginBottom && 'margin-bottom: 4rem'};
		${props.scheme && `color: ${Colors[props.scheme][props.variant || 'base']};`}
	`};
`;

Heading.H1 = styled.h1`
	${baseStyle}
	font-size: 7rem;
`;
Heading.H2 = styled.h2`
	${baseStyle}
	font-size: 5.5rem;
`;
Heading.H3 = styled.h3`
	${baseStyle}
	font-size: 5rem;
`;
Heading.H4 = styled.h4`
	${baseStyle}
	font-size: 4.5rem;
`;
Heading.H5 = styled.h5`
	${baseStyle}
	font-size: 3rem;
`;
Heading.H6 = styled.h6`
	${baseStyle}
	font-size: 2rem;
	line-height: 1;
`;

export default Heading;
