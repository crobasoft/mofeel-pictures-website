import React from 'react';
import PropTypes from 'prop-types';

import { ListItem } from './IconList.style';

const IconListItem = ({ children, icon, ...props }) => (
	<ListItem {...props}>
		<ListItem.Icon className={icon} />
		<ListItem.Text>{ children }</ListItem.Text>
	</ListItem>
);
IconListItem.propTypes = {
	children: PropTypes.node,
	icon: PropTypes.string,
};
export default IconListItem;
