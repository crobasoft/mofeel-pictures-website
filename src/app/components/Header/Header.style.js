import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
	${props => css`
		position: relative;
		height: auto;
		${!props.hasVideo && 'margin-bottom: 6rem;'}
	`}
`;

export const Content = styled.div`
	${props => css`
		${props.hasVideo && `
			position: absolute;
			top: 0;
			left: 0;
			height: 100%;
			width: 100%;
			z-index: 5;
			display: flex;
			flex-direction: column;
		`}
	`}
`;

export const NestedContent = styled.div`
	position: ${props => (props.hasVideo ? 'absolute' : 'relative')};
	left: 0;
	top: 0;
	height: 100%;
	width: 100%;
	display: flex;
	align-items: center;
`;

export const Navigation = styled.div`
	display: flex;
	align-items: center;
	position: relative;
	z-index: 50;
	padding: 0 4rem;
	@media (max-width: 1000px) {
		flex-direction: column;
	}
`;

export const NavigationExtraContainer = styled.div`
	flex-grow: 1;
`;

export const NavigationLogoContainer = styled.div`
	text-align: center;
	padding-top: 3rem;
	transition: 0.3s ease-out;
	@media (max-width: 550px) {
		width: 60% !important;
	}
	@media (min-width: 551px) and (max-width: 1000px) {
		width: 40% !important;
	}
	@media (min-width: 1260px) {
	}
`;

export const NavigationLinks = styled.div`
	display: none;
	align-items: center;
	flex-grow: 1;
	justify-content: flex-end;
	@media (min-width: 550px) {
		display: flex;
	}
	@media (max-width: 1000px) {
		width: 100% !important;
		justify-content: center;
		margin-top: 4rem;
	}
`;

export const LinkItem = styled.div`
	padding: 1rem 1.5rem;
`;

export const VideoWrapper = styled.div`
	z-index: 0;
	position: relative;
`;
