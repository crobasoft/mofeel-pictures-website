const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
const port = 1234;

app.use(bodyParser.urlencoded({ extended: true }));
app.use('/dist', express.static('dist'));
app.use(express.static('dist'));

app.get('/*', (req, res) => {
	res.sendFile(path.resolve(`${__dirname}/../dist/index.html`));
});

app.listen(port, () => {
	console.info(`Mofeel Pictures MockServer is now running on port ${port}`); // eslint-disable-line
});
