import styled from 'styled-components';

const Flex = styled.div`
	display: flex;
	flex-direction: column;
	${props => props.justifyContent && `justify-content: ${props.justifyContent};`}
	${props => props.alignItems && `align-items: ${props.alignItems};`}

	@media (min-width: 500px) {
		flex-direction: row;
	}
`;

Flex.Box = styled.div`
	${props => props.grow && `flex-grow: ${props.grow};`}
	${props => props.padded && 'padding: 4rem;'}
	${props => props.paddedLeft && 'padding-left: 4rem;'}
	${props => props.paddedRight && 'padding-right: 4rem;'}
	${props => props.paddedTop && 'padding-top: 4rem;'}
	${props => props.paddedBottom && 'padding-bottom: 4rem;'}
`;

export default Flex;
