import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import ModalVideo from 'react-modal-video';

import { Icon, Heading } from 'Elements';
import VimeoThumbnail from './VimeoThumbnail';
import { VimeoAlbumContainer, LoadingContainer, ErrorContainer } from './VimeoAlbum.style';

class VimeoAlbum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			videos: null,
			error: false,
			modalIsOpen: false,
			currentVideoId: null,
		};
		this.isActive = true;
	}

	componentDidMount() {
		if (this.props.albumId) {
			this.fetchAlbum(this.props.albumId);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.albumId !== this.props.albumId) {
			this.fetchAlbum(nextProps.albumId);
		}
	}

	componentWillUnmount() {
		this.isActive = false;
	}

	onVideoSelect = (videoId) => {
		this.updateState({ currentVideoId: videoId });
		this.openModal();
	}

	getThumbnail = imageArray => {
		const img = imageArray[3] || imageArray[imageArray.length - 1];
		return img.link;
	}

	openModal = () => {
		this.updateState({ modalIsOpen: true });
	}

	closeModal = () => {
		this.updateState({ modalIsOpen: false });
	}

	updateState = ({ ...newState }) => {
		if (this.isActive) {
			this.setState({ ...newState });
		}
	};

	extractVideoIdFromURI = (videoUri) => videoUri.substring(videoUri.lastIndexOf('/') + 1);

	fetchAlbum = albumId => {
		this.updateState({
			videos: null,
			isLoading: true,
			error: false,
		});
		return axios.get(
			`https://api.vimeo.com/users/mofeelpictures/albums/${albumId}/videos`,
			{ headers: { Authorization: 'Bearer 779aac1c3f3d8a0969f17d43004d81d4' } },
		)
			.then((response) => {
				const vimeoReponse = response.data;
				if (vimeoReponse) {
					this.updateState({ videos: vimeoReponse.data, isLoading: false });
				}
			})
			.catch(() => {
				this.updateState({ error: true, isLoading: false });
			});
	}

	render() {
		const { isLoading, videos, error } = this.state;

		if (error) {
			return (
				<ErrorContainer>
					<Heading.H6 thin>Ett fel uppstod vid laddningen av filmerna</Heading.H6>
				</ErrorContainer>
			);
		}

		return (
			<VimeoAlbumContainer isLoading={isLoading}>
				<ModalVideo
					channel="vimeo"
					isOpen={this.state.modalIsOpen}
					videoId={this.state.currentVideoId}
					onClose={this.closeModal}
				/>
				{isLoading &&
					<LoadingContainer>
						<Icon
							icon="fas fa-spinner"
							spin
						/>
						<Heading.H6 thin>LADDAR</Heading.H6>
					</LoadingContainer>
				}
				{videos && videos.slice(0, this.props.maxVideos).map(video => (
					<VimeoThumbnail
						key={this.extractVideoIdFromURI(video.uri)}
						title={!this.props.hideTitles ? video.name : null}
						imgSrc={this.getThumbnail(video.pictures.sizes)}
						onSelect={() => this.onVideoSelect(this.extractVideoIdFromURI(video.uri))}
					/>
				))}
			</VimeoAlbumContainer>
		);
	}
}
VimeoAlbum.propTypes = {
	albumId: PropTypes.string,
	maxVideos: PropTypes.number,
	hideTitles: PropTypes.bool,
};
VimeoAlbum.defaultProps = {
	maxVideos: 999,
};
export default VimeoAlbum;
