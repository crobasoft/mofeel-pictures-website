import React, { Component } from 'react';
import { Wrapper, ArrowContainer, Arrow, ArrowText } from './ScrollUpArrow.style';

class ScrollUpArrow extends Component {
	scrollToTop = () => {
		window.scrollTo(0, 0);
	};

	render() {
		return (
			<Wrapper>
				<ArrowContainer onClick={this.scrollToTop}>
					<ArrowText>Kom upp igen</ArrowText>
					<Arrow icon="fas fa-arrow-up animated fadeInUp" />
				</ArrowContainer>
			</Wrapper>
		);
	}
}
export default ScrollUpArrow;
