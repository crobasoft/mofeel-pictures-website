import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ScrollUpArrow from 'Components/ScrollUpArrow';

import ViewStyle from './View.style';

class View extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showScrollUpArrow: false,
		};
	}

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
	}

	handleScroll = () => {
		if (window && window.scrollY >= 700 && !this.state.showScrollUpArrow) {
			this.setState({ showScrollUpArrow: true });
		} else if (window && window.scrollY < 700 && this.state.showScrollUpArrow) {
			this.setState({ showScrollUpArrow: false });
		}
	};

	render() {
		return (
			<ViewStyle onScroll={this.handleScroll}>
				{this.state.showScrollUpArrow &&
					<ScrollUpArrow />
				}
				{this.props.children}
			</ViewStyle>
		);
	}
}
View.propTypes = {
	children: PropTypes.node,
};
export default View;
