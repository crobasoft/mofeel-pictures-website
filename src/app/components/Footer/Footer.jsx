import React from 'react';

import { Logo, TextLink } from 'Elements';
import {
	Wrapper,
	ContactContainer,
	LogoContainer,
	SocialMediaContainer,
} from './Footer.style';

const Footer = () => (
	<Wrapper className="row">
		<ContactContainer className="five columns">
			<div>
				<p>
					TURINGEVÄGEN 41b
					<br />
					125 43 ÄLVSJÖ
				</p>
			</div>
			<div>
				<p>
					+46 76 708 28 46
					<br />
					hej@mofeelpictures.se
				</p>
			</div>
		</ContactContainer>
		<LogoContainer className="two columns">
			<Logo />
		</LogoContainer>
		<SocialMediaContainer className="five columns">
			<TextLink href="http://facebook.com/mofeelpictures">
				FACEBOOK
			</TextLink>
			<TextLink href="https://www.instagram.com/mofeelpictures/">
				INSTAGRAM
			</TextLink>
			<TextLink href="https://twitter.com/mofeelpictures">
				TWITTER
			</TextLink>
			<TextLink href="https://vimeo.com/mofeelpictures/">
				VIMEO
			</TextLink>
		</SocialMediaContainer>
	</Wrapper>
);
export default Footer;
