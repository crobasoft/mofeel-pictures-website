import React, { Component } from 'react';
import Lottie from 'react-lottie';
import PropTypes from 'prop-types';
import * as animationData from './animation.json';

import { Wrapper } from './StartAnimation.style';

class StartAnimation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isComplete: false,
			fadingOut: false,
		};
		this.isActive = false;
	}

	componentDidMount() {
		this.isActive = true;
		setInterval(() => {
			if (this.isActive) {
				this.setState({ fadingOut: true });
			}
		}, 2000);
	}

	componentWillUnmount() {
		this.isActive = false;
	}

	animationComplete = () => {
		if (this.props.onComplete) {
			this.props.onComplete();
		}
		if (this.isActive) {
			this.setState({ isComplete: true });
		}
	}

	render() {
		return !this.state.isComplete ? (
			<Wrapper className={`animated ${this.state.fadingOut && 'fadeOut'}`}>
				<Lottie
					options={{
						loop: false,
						autoplay: true,
						animationData,
					}}
					eventListeners={
						[
							{
								eventName: 'complete',
								callback: this.animationComplete,
							},
						]
					}
				/>
			</Wrapper>
		) : null;
	}
}
StartAnimation.propTypes = {
	onComplete: PropTypes.func,
};
export default StartAnimation;
