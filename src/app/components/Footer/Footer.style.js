import styled from 'styled-components';
import { Typography, Colors } from 'Styles';

export const Wrapper = styled.footer`
	position: relative;
	height: auto;
	display: flex;
	flex-direction: column;
	align-items: center;
	max-width: 150rem;
	margin: 0 auto;
	border-top: 1px solid ${Colors.default.base};
	padding: 4rem 0;

	@media (min-width: 1000px) {
		flex-direction: row;
		border: 0;
	}

	p {
		font-weight: ${Typography.weights.light};
		font-size: 1.4rem;
		margin: 0;
	}
`;

export const ContactContainer = styled.div`
	order: 2;
	display: flex;
	align-items: center;
	justify-content: space-evenly;
	div {
		padding: 1rem;
	}

	@media (min-width: 1000px) {
		order: 0;
	}
`;

export const LogoContainer = styled.div`
	order: 0;
	text-align: center;
	align-items: center;
	img {
		max-width: 20rem;
		padding: 2rem;
	}

	@media (max-width: 1000px) {
		width: 100% !important;
	}

	@media (min-width: 1000px) {
		order: 1;
	}
`;

export const SocialMediaContainer = styled.div`
	order: 1;
	display: flex;
	align-items: center;
	justify-content: space-evenly;
	a {
		font-size: 1.4rem;
		text-align: center;
		margin: 1.5rem;
	}

	@media (min-width: 1000px) {
		order: 2;
	}
`;
