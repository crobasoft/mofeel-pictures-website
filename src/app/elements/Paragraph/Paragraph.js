import styled from 'styled-components';
import { Colors, Typography } from 'Styles';

const Paragraph = styled.p`
	font-family: ${Typography.fonts.base};
	margin-bottom: ${props => (props.noMargin ? '0' : '2rem')};
	${props => props.bold && `font-weight: ${Typography.weights.bold};`}
	${props => props.light && `font-weight: ${Typography.weights.light};`}
	${props => props.thin && `font-weight: ${Typography.weights.thin};`}
	${props => props.small && 'font-size: 1.5rem;'}
	${props => props.scheme && `color: ${Colors[props.scheme][props.variant || 'base']};`}
`;

export default Paragraph;
