/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';
import PropTypes from 'prop-types';

import {
	VideoBackgroundStyle,
	VideoBackgroundOverlay,
	VideoStyle,
} from './VideoBackground.style';

const VideoBackground = ({
	videoId,
}) => (
	<VideoBackgroundStyle>
		<VideoBackgroundOverlay />
		<VideoStyle
			title={videoId}
			src={`https://player.vimeo.com/video/${videoId}?background=1&loop=1`}
			width="960"
			height="540"
			frameBorder="0"
			webkitallowfullscreen
			mozallowfullscreen
			allowFullscreen
		/>
	</VideoBackgroundStyle>
);

VideoBackground.propTypes = {
	videoId: PropTypes.string,
};

export default VideoBackground;
