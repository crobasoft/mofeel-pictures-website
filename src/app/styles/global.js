/**
 * NOTE: This file must only be imported once in the project
 */
import { injectGlobal } from 'styled-components';
import { Typography, Colors } from 'Styles';

export default injectGlobal`
	html {
		@media (max-width: 500px) {
			font-size: 52%;
		}
	}
	body {
		font-family: ${Typography.fonts.base};
		font-size: ${Typography.sizes.text.base};
		color: ${Colors.text.base};
		line-height: 1.5;
	}

	iframe {
		border: none;
	}

	*, *:after, *:before {
		-webkit-box-sizing: border-box;
	    box-sizing: border-box;
	}
`;
